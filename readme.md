# Worker Queues Symfony

## Prerequisites

### MacOS

HomeBrew
```
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Docker
```
$ brew install --cask docker
```

## Installation

Run docker-compose up
```
$ docker-compose up --build -d
```

Open Bash in container
```
$ docker exec -it php /bin/bash
```

Generate Symfony Flex project
```
$ symfony new .
```

Your Symfony app is now mounted in the `/app` folder.

To stop the container from running, you could run the following:
```
$ docker-compose down
```

## Usage

If you want to install a new package in your Symfony project, run the following example inside the container:
```
$ composer require annotations
```

For installation of all packages run the following inside the container:
```
$ composer install
```

Your Symfony project is available at `localhost`. The database is available at `localhost:3036`

--------
## Kubernetes

Minikube
```
$ brew install minikube
```

Use Docker commands inside Kubernetes Cluster
```
$ eval $(minikube -p minikube docker-env)
```

Start the Cluster (Change mount-string to local path)
```
$ minikube start --mount-string="$HOME/{Path to installation}/symfony-worker-queue/app:/app" --mount
```

Inside the local installation of this repostority run this to build a Docker image inside the Kubernetes Cluster
```
$ docker build -t symfony-worker-queue/php .
```

Now that you have all images available in the cluster you can apply the configs inside the cluster
```
$ kubectl apply -f web-pod.yml database.yml
```

At last you can migrate the database by entering the Shell of the Web Pod.
```
$ kubectl exec -it web -c php -- /bin/bash
$ bin/console doctrine:migrations:migrate
```

To port forward the Web pod you can use the following command
```
$ kubectl port-forward pods/web 8080:8080
```

To start the Minikube Dashboard
```
$ minikube dashboard
```

## Workers

Use Docker commands inside Kubernetes Cluster
```
$ eval $(minikube -p minikube docker-env)
```

Build Worker Docker File
```
$ docker build -f worker-Dockerfile -t symfony-worker-queue/worker .
```         

Apply all the configs in the Cluster
```
$ kubectl apply -f rabbitmq.yml worker-pod.yml autoscaler.yml
```
