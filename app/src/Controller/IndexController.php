<?php

namespace App\Controller;

use App\Repository\GuestRepository;
use App\Repository\RegistrationNumberRepository;
use App\Repository\ReservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(): Response
    {
        return $this->render('index/index.html.twig');
    }

    #[Route('/reset', name: 'app_reset', methods: ['DELETE'])]
    public function resetAction(
        GuestRepository $guestRepository,
        ReservationRepository $reservationRepository,
        RegistrationNumberRepository $registrationNumberRepository
    ): Response
    {
        $registrationNumberRepository->removeAll();
        $guestRepository->removeAll();
        $reservationRepository->removeAll();

        return $this->json(null);
    }
}
