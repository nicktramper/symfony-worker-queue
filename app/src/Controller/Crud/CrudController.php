<?php

namespace App\Controller\Crud;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class CrudController extends RestController
{
    public function __construct(
        private string $class,
        private ServiceEntityRepositoryInterface $repository,
        ValidatorInterface $validator
    ) {
        parent::__construct($validator);
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function indexAction(): Response
    {
        $entities = $this->repository->findAll();

        return $this->json($entities);
    }

    #[Route('/{id}', name: 'find', methods: ['GET'])]
    public function findAction(int $id): Response
    {
        $entity = $this->repository->find($id);

        return $this->json($entity);
    }

    #[Route('/', name: 'create', methods: ['POST'])]
    public function createAction(Request $request, EntityManagerInterface $em): Response
    {
        $entity = $this->deserialize($request->getContent(), $this->class);

        try {
            $this->validate($entity);
        } catch (Exception $exception) {
            return $this->json($exception->getMessage(), $exception->getCode());
        }

        $em->persist($entity);

        $em->flush();

        return $this->json($entity);
    }

    #[Route('/{id}', name: 'update', methods: ['PUT'])]
    public function updateAction(Request $request, int $id, EntityManagerInterface $em): Response
    {
        $entity = $this->repository->find($id);

        $this->deserialize($request->getContent(), $this->class, $entity);

        try {
            $this->validate($entity);
        } catch (Exception $exception) {
            return $this->json($exception->getMessage(), $exception->getCode());
        }

        $em->flush();

        return $this->json($entity);
    }

    #[Route('/{id}', name: 'delete', methods: ['DELETE'])]
    public function deleteAction(int $id, EntityManagerInterface $em): Response
    {
        $entity = $this->repository->find($id);

        $em->remove($entity);
        $em->flush();

        return $this->json($entity);
    }
}