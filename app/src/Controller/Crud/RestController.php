<?php

namespace App\Controller\Crud;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RestController extends AbstractController
{
    private SerializerInterface $serializer;

    public function __construct(
        private ValidatorInterface $validator
    ) {
        $encoders = [new JsonEncoder()];
        $extractor = new PropertyInfoExtractor([], [new PhpDocExtractor(), new ReflectionExtractor()]);
        $normalizers = [new ArrayDenormalizer(), new ObjectNormalizer(null, null, null, $extractor)];

        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * Deserialize JSON to an object of the given class.
     *
     * @param string $data
     * @param string $class
     * @param mixed|null $entity
     *
     * @return mixed
     */
    protected function deserialize(string $data, string $class, mixed $entity = null): mixed
    {
        $options = [];

        if (isset($entity)) {
            $options[AbstractNormalizer::OBJECT_TO_POPULATE] = $entity;
        }

        return $this->serializer->deserialize(
            $data,
            $class,
            'json',
            $options
        );
    }

    /**
     * Decodes the given JSON into an array.
     *
     * @param string $json
     * @return array|null
     */
    protected function jsonDecode(string $json): ?array
    {
        $data = json_decode($json, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadRequestHttpException('Invalid JSON request body');
        }

        return $data;
    }

    /**
     * Validate Entity
     *
     * @throws Exception
     */
    protected function validate(mixed $entity): void
    {
        $errors = $this->validator->validate($entity);

        if (count($errors) > 0) {
            $errorsString = (string) $errors;

            throw new Exception($errorsString, 400);
        }
    }

    protected function json(mixed $data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        if (isset($context['groups'])) {
            $context['groups'][] = 'default';
        } else {
            $context['groups'] = ['default'];
        }

        return parent::json($data, $status, $headers, $context);
    }
}