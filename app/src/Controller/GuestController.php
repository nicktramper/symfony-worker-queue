<?php

namespace App\Controller;

use App\Controller\Crud\CrudController;
use App\Entity\Guest;
use App\Manager\GuestManager;
use App\Message\UpdateGuestMessage;
use App\Repository\GuestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/guest', name: 'guest_')]
class GuestController extends CrudController
{
    public function __construct(
        private GuestRepository $guestRepository,
        private GuestManager $guestManager,
        ValidatorInterface $validator
    ) {
        parent::__construct(Guest::class, $this->guestRepository, $validator);
    }

    #[Route('/', name: 'create', methods: ['POST'])]
    public function createAction(Request $request, EntityManagerInterface $em): Response
    {
        $data = $this->jsonDecode($request->getContent());

        $entity = new Guest();

        $this->guestManager->populateGuest($entity, $data);

        try {
            $this->validate($entity);
        } catch (Exception $exception) {
            return $this->json($exception->getMessage(), $exception->getCode());
        }

        $em->persist($entity);

        $em->flush();

        return $this->json($entity);
    }

    #[Route('/{id}', name: 'update', methods: ['PUT'])]
    public function updateAction(Request $request, int $id, EntityManagerInterface $em): Response
    {
        $data = $this->jsonDecode($request->getContent());

        $entity = $this->guestRepository->find($id);

        $this->guestManager->populateGuest($entity, $data);

        try {
            $this->validate($entity);
        } catch (Exception $exception) {
            return $this->json($exception->getMessage(), $exception->getCode());
        }

        $em->flush();

        return $this->json($entity);
    }

    #[Route('/sync/{id}', name: 'update_sync', methods: ['PUT'])]
    public function updateSyncAction(
        Request $request,
        int $id,
        EntityManagerInterface $em,
    ): Response
    {
        $data = $this->jsonDecode($request->getContent());

        $entity = $this->guestRepository->find($id);

        $this->guestManager->populateGuest($entity, $data);

        $this->guestManager->updateAllReservationRegistrationNumbers($entity);

        try {
            $this->validate($entity);
        } catch (Exception $exception) {
            return $this->json($exception->getMessage(), $exception->getCode());
        }

        $em->flush();

        return $this->json($entity);
    }

    #[Route('/async/{id}', name: 'update_async', methods: ['PUT'])]
    public function updateAsyncAction(
        Request $request,
        int $id,
        EntityManagerInterface $em,
        MessageBusInterface $bus
    ): Response
    {
        $data = $this->jsonDecode($request->getContent());

        $entity = $this->guestRepository->find($id);

        $this->guestManager->populateGuest($entity, $data);

        try {
            $this->validate($entity);
        } catch (Exception $exception) {
            return $this->json($exception->getMessage(), $exception->getCode());
        }

        $em->flush();

        $bus->dispatch(new UpdateGuestMessage($entity->getId()));

        return $this->json($entity);
    }
}