<?php

namespace App\Controller;

use App\Controller\Crud\CrudController;
use App\Entity\Reservation;
use App\Manager\ReservationManager;
use App\Repository\ReservationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/reservation', name: 'reservation_')]
class ReservationController extends CrudController
{
    public function __construct(
        private ReservationRepository $reservationRepository,
        private ReservationManager $reservationManager,
        ValidatorInterface $validator
    ) {
        parent::__construct(Reservation::class, $this->reservationRepository, $validator);
    }

    #[Route('/', name: 'create', methods: ['POST'])]
    public function createAction(Request $request, EntityManagerInterface $em): Response
    {
        $data = $this->jsonDecode($request->getContent());

        $entity = new Reservation();

        $this->reservationManager->populateReservation($entity, $data);

        try {
            $this->validate($entity);
        } catch (Exception $exception) {
            return $this->json($exception->getMessage(), $exception->getCode());
        }

        $em->persist($entity);

        $em->flush();

        return $this->json($entity);
    }

    #[Route('/{id}', name: 'update', methods: ['PUT'])]
    public function updateAction(Request $request, int $id, EntityManagerInterface $em): Response
    {
        $data = $this->jsonDecode($request->getContent());

        $entity = $this->reservationRepository->find($id);

        $this->reservationManager->populateReservation($entity, $data);

        try {
            $this->validate($entity);
        } catch (Exception $exception) {
            return $this->json($exception->getMessage(), $exception->getCode());
        }

        $em->flush();

        return $this->json($entity);
    }
}