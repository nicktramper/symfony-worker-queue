<?php

namespace App\Manager;

use App\Entity\Guest;
use App\Entity\RegistrationNumber;
use App\Repository\ReservationRepository;
use Doctrine\ORM\EntityManagerInterface;

class GuestManager
{
    public function __construct(
        private EntityManagerInterface $em,
        private ReservationRepository $reservationRepository,
        private ReservationManager $reservationManager
    ) {}

    /**
     * Populate the guest data with the request data.
     *
     * @param Guest $guest
     * @param array $data
     * @return void
     */
    public function populateGuest(Guest $guest, array $data)
    {
        if (isset($data['name'])) {
            $guest->setName($data['name']);
        }

        if (isset($data['registrationNumbers']) && is_array($data['registrationNumbers'])) {
            $this->updateRegistrationNumbers($guest, $data['registrationNumbers']);
        }
    }

    /**
     * Update all the registrationNumbers on all the reservations
     *
     * @param Guest $guest
     * @return void
     */
    public function updateAllReservationRegistrationNumbers(Guest $guest)
    {
        $reservations = $this->reservationRepository->findBy(['guest' => $guest]);

        foreach ($reservations as $reservation) {
            foreach ($guest->getRegistrationNumbers() as $registrationNumber) {
                if ($this->reservationManager->isRegistrationNumberInReservation($reservation, $registrationNumber->getNumber())) {
                    continue;
                }

                $reservation->addRegistrationNumber($registrationNumber);
            }
        }
    }

    /**
     * UpdateRegistrationNumbers merges the new RegistrationNumbers with the existing RegistrationNumbers.
     *
     * @param Guest $guest
     * @param array $registrationNumbers
     * @return void
     */
    private function updateRegistrationNumbers(Guest $guest, array $registrationNumbers)
    {
        $guest->getRegistrationNumbers()->clear();

        foreach ($registrationNumbers as $item) {
            if ($this->isRegistrationNumberInGuest($guest, $item['number'])) {
                continue;
            }

            $registrationNumber = new RegistrationNumber();
            $registrationNumber->setNumber($item['number']);

            $this->em->persist($registrationNumber);

            $guest->addRegistrationNumber($registrationNumber);
        }
    }

    /**
     * ExistsRegistrationNumberInGuest checks if number already existst on the Guest.
     *
     * @param Guest $guest
     * @param string $number
     * @return bool
     */
    private function isRegistrationNumberInGuest(Guest $guest, string $number): bool
    {
        return in_array($number, $guest->getRegistrationNumbers()->getValues());
    }
}