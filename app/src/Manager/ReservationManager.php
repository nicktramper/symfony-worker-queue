<?php

namespace App\Manager;

use App\Entity\Reservation;
use App\Repository\GuestRepository;

class ReservationManager
{
    public function __construct(
        private GuestRepository $guestRepository
    ) {}

    public function populateReservation(Reservation $reservation, array $data)
    {
        if (isset($data['guest']) && isset($data['guest']['id']) ) {
            $guest = $this->guestRepository->find($data['guest']['id']);

            $reservation->setGuest($guest);
        }
    }

    /**
     * ExistsRegistrationNumberInGuest checks if number already existst on the Reservation.
     *
     * @param Reservation $reservation
     * @param string $number
     * @return bool
     */
    public function isRegistrationNumberInReservation(Reservation $reservation, string $number): bool
    {
        return in_array($number, $reservation->getRegistrationNumbers()->getValues());
    }
}