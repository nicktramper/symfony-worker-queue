<?php

namespace App\Message;

class UpdateGuestMessage
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getContent(): int
    {
        return $this->id;
    }

}