<?php

namespace App\Entity;

use App\Repository\RegistrationNumberRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RegistrationNumberRepository::class)]
class RegistrationNumber
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('default')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Reservation::class, inversedBy: 'registrationNumbers')]
    #[ORM\JoinColumn(nullable: true)]
    private $reservation;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('default')]
    private $number;

    #[ORM\Column(type: 'datetime')]
    #[Groups('default')]
    private $created;

    #[ORM\ManyToOne(targetEntity: Guest::class, inversedBy: 'registrationNumbers')]
    #[ORM\JoinColumn(nullable: true)]
    private $guest;

    public function __construct()
    {
        $this->created = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function setReservation(?Reservation $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getGuest(): ?Guest
    {
        return $this->guest;
    }

    public function setGuest(?Guest $guest): self
    {
        $this->guest = $guest;

        return $this;
    }
}