<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('default')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'reservation', targetEntity: RegistrationNumber::class, orphanRemoval: true)]
    #[Groups('default')]
    private $registrationNumbers;

    #[ORM\ManyToOne(targetEntity: Guest::class, inversedBy: 'reservations')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'cascade')]
    #[Groups('default')]
    private $guest;

    public function __construct()
    {
        $this->registrationNumbers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, RegistrationNumber>
     */
    public function getRegistrationNumbers(): Collection
    {
        return $this->registrationNumbers;
    }

    public function addRegistrationNumber(RegistrationNumber $registrationNumber): self
    {
        if (!$this->registrationNumbers->contains($registrationNumber)) {
            $this->registrationNumbers[] = $registrationNumber;
            $registrationNumber->setReservation($this);
        }

        return $this;
    }

    public function removeRegistrationNumber(RegistrationNumber $registrationNumber): self
    {
        if ($this->registrationNumbers->removeElement($registrationNumber)) {
            // set the owning side to null (unless already changed)
            if ($registrationNumber->getReservation() === $this) {
                $registrationNumber->setReservation(null);
            }
        }

        return $this;
    }

    public function getGuest(): ?Guest
    {
        return $this->guest;
    }

    public function setGuest(?Guest $guest): self
    {
        $this->guest = $guest;

        return $this;
    }
}