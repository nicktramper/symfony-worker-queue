<?php

namespace App\Entity;

use App\Repository\GuestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: GuestRepository::class)]
class Guest
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('default')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Groups('default')]
    private $name;

    #[ORM\OneToMany(mappedBy: 'guest', targetEntity: RegistrationNumber::class, orphanRemoval: true)]
    #[Groups('default')]
    private $registrationNumbers;

    #[ORM\OneToMany(mappedBy: 'guest', targetEntity: Reservation::class, orphanRemoval: true)]
    private $reservations;

    public function __construct()
    {
        $this->registrationNumbers = new ArrayCollection();
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, RegistrationNumber>
     */
    public function getRegistrationNumbers(): Collection
    {
        return $this->registrationNumbers;
    }

    public function addRegistrationNumber(RegistrationNumber $registrationNumber): self
    {
        if (!$this->registrationNumbers->contains($registrationNumber)) {
            $this->registrationNumbers[] = $registrationNumber;
            $registrationNumber->setGuest($this);
        }

        return $this;
    }

    public function removeRegistrationNumber(RegistrationNumber $registrationNumber): self
    {
        if ($this->registrationNumbers->removeElement($registrationNumber)) {
            // set the owning side to null (unless already changed)
            if ($registrationNumber->getGuest() === $this) {
                $registrationNumber->setGuest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setGuest($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getGuest() === $this) {
                $reservation->setGuest(null);
            }
        }

        return $this;
    }
}