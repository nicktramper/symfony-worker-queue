<?php

namespace App\Repository;

use App\Entity\RegistrationNumber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegistrationNumber|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegistrationNumber|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegistrationNumber[]    findAll()
 * @method RegistrationNumber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistrationNumberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistrationNumber::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(RegistrationNumber $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(RegistrationNumber $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     *  Remove all RegistrationNumbers
     * 
     * @return void 
     */
    public function removeAll()
    {
        $qb = $this->createQueryBuilder('t0');

        $qb->delete();

        $qb->getQuery()->execute();
    }
}
