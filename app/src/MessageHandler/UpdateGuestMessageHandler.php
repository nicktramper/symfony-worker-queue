<?php

namespace App\MessageHandler;
use App\Manager\GuestManager;
use App\Message\UpdateGuestMessage;
use App\Repository\GuestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class UpdateGuestMessageHandler
{
    public function __construct(
        private EntityManagerInterface $em,
        private GuestRepository $guestRepository,
        private GuestManager $guestManager
    )
    {}

    public function __invoke(UpdateGuestMessage $message)
    {
        $guest = $this->guestRepository->find($message->getContent());

        if (!$guest) {
            return;
        }

        $this->guestManager->updateAllReservationRegistrationNumbers($guest);

        $this->em->flush();
    }
}