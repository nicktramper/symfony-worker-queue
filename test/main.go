package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/joho/godotenv"
)

type Guest struct {
	Name                string
	RegistrationNumbers []struct {
		Number string
	}
}

type Request struct {
	URL  string
	Body []byte
}

var client http.Client

func main() {
	client = http.Client{}

	guest := ReadGuestJSON()

	godotenv.Load(".env")
	routes := strings.Fields(os.Getenv("ROUTES"))

	intVar, err := strconv.Atoi(os.Getenv("CALL_AMOUNT"))
	if err != nil {
		panic(err)
	}

	ch := make(chan string)

	for _, route := range routes {
		go MakeRequests(Request{
			URL:  route,
			Body: guest,
		}, ch, intVar)
	}

	for range routes {
		fmt.Println(<-ch)
	}

	defer Disengage()
}

func Disengage() {
	req, _ := http.NewRequest(http.MethodDelete, os.Getenv("RESET_URL"), bytes.NewBuffer(nil))

	client.Do(req)
}

func MakeRequests(request Request, ch chan<- string, amount int) {
	start := time.Now()

	for i := 0; i < amount; i++ {
		MakeRequest(request)
	}

	secs := time.Since(start).Milliseconds()

	ch <- fmt.Sprintf("%v ms elapsed while responding %s", secs, request.URL)
}

func MakeRequest(request Request) {
	json, err := json.Marshal(request.Body)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest(http.MethodPut, request.URL, bytes.NewBuffer(json))

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	ioutil.ReadAll(resp.Body)
}

func ReadGuestJSON() []byte {
	data, err := os.ReadFile("guest.json")
	if err != nil {
		log.Fatal(err)
	}

	return data
}
